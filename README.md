# IUECar Rentals

## Descripcion del negocio 
IUECar Rentals Es una empresa especializada en el alquiler de automoviles,furgonetas y camiones. los vehiculos pueden ser rentados con o sin conductor, a su vez los conductores pueden ser asignados a un vehiculo de IUECar Rentals o como empleados temporales de otras compañias.
IUECar Rentals en la actualidad cuenta con mas de 10.000 vehiculos y 30.000 conductores a nivel mundial.

## Requerimiento
### Contexto del requerimiento IUECar Rentals
IUECar Rentals  tiene la necesidad de actualizar los sistemas de gestion empresarial para su parque automotor y sus empleados  a nivel mundial, para esto se requiere contar una solución tecnológica que le permita centralizar,gestionar y proyectar todos los componentes que se articulan en la tenencia de dichos servicios a nivel del seguro obligatorio de los vehiculos y licencias de conducion de los conductores.

A nivel general las acciones que se ejecuten en la aplicación deberán estar enfocadas a poder controlar las fechas de vencimiento de los seguros obligatorios y fechas de vencimiento  de las licencias de conducion.
la aplicación se integrara a un sistemas interno(SOAP WEB SERVICE) que le entregaran el complemento de características para enriquecer la gestión (sistema de facturacion, sistema de personal, sistema de activos).

El sistema propuesto deberá poder cargar la información suministrada por el sistema (SOAP WEB SERVICE) ya existente (datos del personal y datos del vehiculo) ,esta información es entregada en formato XML . 

### Información del vehiculo tiene la siguiente estructura:

| Nombre        |          Tipo  |      Descripcion | 
|:-------------:|:--------------:|:----------------:| 
| Placa         |         String |                  |
|               |                |                  | 
| Tipo vehículo |         String |                  |  
|               |                |                  | 
| estado        |         Bolean |                  |   


### Informacion del conductor

| Nombre        |          Tipo  |      Descripcion |   
|:-------------:|:--------------:|:----------------:|
| cedula        |         int    |                  |
|               |                |                  |  
| nombre        |         String |                  |  
|               |                |                  | 
| estado        |         Bolean |                  |   
|               |                |                  |  
| pais          |         String |                  |   
|               |                |                  | 
| ciudad        |        String  |                  |   



Esta información se deberá cargara en el sistema a desarrollar para completar la información de fecha de vencimiento seguro obilgatorio de los vehiculos y fecha de vencimiento de la  licencia de conduccion de los conductores. y presentarla a nivel web atra vez de una API REST FULL. 

[![Diagrama-Componentes-drawio.png](https://i.postimg.cc/4NvSyzmJ/Diagrama-Componentes-drawio.png)](https://postimg.cc/qzRLZ6sY)


## Autor
Sebastian castro morales

## Asignatura
Desarrollo de software II


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/scastrom/iuesolution.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/scastrom/iuesolution/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


[def]: https://gitlab.com/scastrom/iuesolution/-/blob/main/Diagramas/Diagrama%20Componentes%20.drawio.svg"Diagrama Componentes"